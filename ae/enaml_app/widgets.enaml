""" themed widgets and other useful helper widgets for your enaml application. """
from functools import partial

from enaml.colorext import Color
from enaml.core.api import Looper
from enaml.layout.api import hbox
from enaml.widgets.api import ColorDialog, Container, Field, MainWindow, PushButton, Slider
from enaml.widgets.popup_view import PopupView

from ae.base import NAME_PARTS_SEP
from ae.gui_app import (
    MIN_FONT_SIZE, MAX_FONT_SIZE,
    THEME_DARK_BACKGROUND_COLOR, THEME_DARK_FONT_COLOR, THEME_LIGHT_BACKGROUND_COLOR, THEME_LIGHT_FONT_COLOR,
    FLOW_KEY_SEP,
    id_of_flow, flow_key_split
    )

from .functions import enaml_rgba


enamldef ThemeContainer(Container):
    foreground << enaml_rgba(*(THEME_LIGHT_FONT_COLOR if app.app_state_light_theme else THEME_DARK_FONT_COLOR))
    background << enaml_rgba(*(THEME_LIGHT_BACKGROUND_COLOR if app.app_state_light_theme else
                               THEME_DARK_BACKGROUND_COLOR))


enamldef ThemePopup(PopupView):
    foreground << enaml_rgba(*(THEME_LIGHT_FONT_COLOR if app.app_state_light_theme else THEME_DARK_FONT_COLOR))
    background << enaml_rgba(*(THEME_LIGHT_BACKGROUND_COLOR if app.app_state_light_theme else
                               THEME_DARK_BACKGROUND_COLOR))


enamldef ThemeButton(PushButton):
    foreground << enaml_rgba(*(THEME_LIGHT_FONT_COLOR if app.app_state_light_theme else THEME_DARK_FONT_COLOR))
    background << enaml_rgba(*(THEME_LIGHT_BACKGROUND_COLOR if app.app_state_light_theme else
                               THEME_DARK_BACKGROUND_COLOR))
    font << str(int(app.app_state_font_size)) + 'px arial'
    icon_size << int(app.app_state_font_size * 1.5)
    minimum_size << (int(app.app_state_font_size * 1.5), int(app.app_state_font_size * 1.5))


enamldef ThemeField(Field):
    foreground << enaml_rgba(*(THEME_LIGHT_FONT_COLOR if app.app_state_light_theme else THEME_DARK_FONT_COLOR))
    background << enaml_rgba(*(THEME_LIGHT_BACKGROUND_COLOR if app.app_state_light_theme else
                               THEME_DARK_BACKGROUND_COLOR))
    font << str(int(app.app_state_font_size)) + 'px arial'
    minimum_size << (int(app.app_state_font_size * 1.5), int(app.app_state_font_size * 1.5))


enamldef ChangeColorButton(ThemeButton):
    background << enaml_rgba(*getattr(app, 'app_state_' + self.text))
    clicked :: ColorDialog(main_app.framework_win,
                           current_color=enaml_rgba(*getattr(main_app, self.text)),
                           callback=partial(main_app.user_preference_color_selected, self.text)
                           ).exec_()


enamldef FlowButton(ThemeButton):
    """ flow button

    optional widget attributes::

        icon_name: image name of the icon, if not declared then the tap_flow_id will be used as image name.
    """
    attr tap_flow_id: str = ""
    attr tap_kwargs: dict = dict(popup_kwargs=dict(opener=self))
    attr icon_name: str = ""
    icon << main_app.cached_icon(self.icon_name or flow_key_split(self.tap_flow_id)[0], \
                                 app.app_state_font_size, app.app_state_light_theme)
    clicked :: main_app.change_flow(self.tap_flow_id, **self.tap_kwargs)
    tool_tip = self.tap_flow_id.replace(NAME_PARTS_SEP, ' ').replace(FLOW_KEY_SEP, ' @: ')
    status_tip = self.tap_flow_id.replace(NAME_PARTS_SEP, ' ').replace(FLOW_KEY_SEP, ' @: ')


enamldef FlowPopup(ThemePopup):
    """ flow popup """
    attr close_kwargs: dict = dict(flow_id=id_of_flow('', '')) \
        if main_app.flow_path_action(path_index=-2) in ('', 'enter') else {}
    closed :: main_app.change_flow(id_of_flow('close', 'flow_popup'), **self.close_kwargs)


enamldef FontSizeEditPopup(FlowPopup): font_size_edit_popup:
    attr parent_popup_to_close: FlowPopup
    ThemeContainer:
        Looper:
            iterable = range(6)
            FlowButton:
                tap_flow_id = id_of_flow('change', 'font_size')
                tap_kwargs = dict(popups_to_close=(parent_popup_to_close, font_size_edit_popup, ),
                                  font_size=MIN_FONT_SIZE + loop.item * (MAX_FONT_SIZE - MIN_FONT_SIZE) / 5)
                minimum_size = (600, MIN_FONT_SIZE)
                text = f"Aa Bb Cc ({MIN_FONT_SIZE + loop.item * (MAX_FONT_SIZE - MIN_FONT_SIZE) / 5}) Xx Yy Zz"
                font = str(int(MIN_FONT_SIZE + loop.item * (MAX_FONT_SIZE - MIN_FONT_SIZE) / 5)) + 'px arial'


enamldef UserPreferencesPopup(FlowPopup): user_pref_popup:
    #parent_anchor << POSITIONS[parent_box.selected_item]
    #anchor << POSITIONS[view_box.selected_item]
    #arrow_size << sizer.value
    #arrow_edge << edge.selected_item
    #offset << (offset_x.value, offset_y.value)
    ThemeContainer:
        padding = 12
        ChangeColorButton:
            text = 'flow_id_ink'
        ChangeColorButton:
            text = 'flow_path_ink'
        ChangeColorButton:
            text = 'selected_item_ink'
        ChangeColorButton:
            text = 'unselected_item_ink'
        FlowButton:
            tap_flow_id = id_of_flow('edit', 'font_size')
            tap_kwargs = dict(popup_kwargs=dict(parent_popup_to_close=user_pref_popup, opener=self))
        Slider: sound_volume:
            minimum_size << (600, app.app_state_font_size)
            minimum = 0
            maximum = 30
            value = int(app.app_state_sound_volume * 30.0)
            value :: main_app.change_app_state('sound_volume', float(sound_volume.value / 30.0))
        ThemeContainer:
            constraints = [dark_theme_button.width == light_theme_button.width,
                           hbox(dark_theme_button, light_theme_button)]
            padding = 0
            ThemeButton: dark_theme_button:
                text = "dark"
                foreground = enaml_rgba(*THEME_DARK_FONT_COLOR)
                background = enaml_rgba(*THEME_DARK_BACKGROUND_COLOR)
                clicked :: main_app.change_flow(id_of_flow('change', 'light_theme'), light_theme=False)
            ThemeButton: light_theme_button:
                text = "light"
                foreground = enaml_rgba(*THEME_LIGHT_FONT_COLOR)
                background = enaml_rgba(*THEME_LIGHT_BACKGROUND_COLOR)
                clicked :: main_app.change_flow(id_of_flow('change', 'light_theme'), light_theme=True)


enamldef ThemeMainWindow(MainWindow):
    attr app                    #: framework app instance (QtApplication)
    attr main_app               #: main app instance

    foreground << enaml_rgba(*(THEME_LIGHT_FONT_COLOR if app.app_state_light_theme else THEME_DARK_FONT_COLOR))
    background << enaml_rgba(*(THEME_LIGHT_BACKGROUND_COLOR if app.app_state_light_theme else
                               THEME_DARK_BACKGROUND_COLOR))

    activated :: main_app.win_activated(self)
