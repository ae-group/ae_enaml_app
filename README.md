<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.94 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# enaml_app 0.3.28

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_enaml_app/develop?logo=python)](
    https://gitlab.com/ae-group/ae_enaml_app)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_enaml_app/release0.3.27?logo=python)](
    https://gitlab.com/ae-group/ae_enaml_app/-/tree/release0.3.27)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_enaml_app)](
    https://pypi.org/project/ae-enaml-app/#history)

>ae_enaml_app package 0.3.28.

[![Coverage](https://ae-group.gitlab.io/ae_enaml_app/coverage.svg)](
    https://ae-group.gitlab.io/ae_enaml_app/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_enaml_app/mypy.svg)](
    https://ae-group.gitlab.io/ae_enaml_app/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_enaml_app/pylint.svg)](
    https://ae-group.gitlab.io/ae_enaml_app/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_enaml_app)](
    https://gitlab.com/ae-group/ae_enaml_app/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_enaml_app)](
    https://gitlab.com/ae-group/ae_enaml_app/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_enaml_app)](
    https://gitlab.com/ae-group/ae_enaml_app/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_enaml_app)](
    https://pypi.org/project/ae-enaml-app/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_enaml_app)](
    https://gitlab.com/ae-group/ae_enaml_app/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_enaml_app)](
    https://libraries.io/pypi/ae-enaml-app)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_enaml_app)](
    https://pypi.org/project/ae-enaml-app/#files)


## installation


execute the following command to install the
ae.enaml_app package
in the currently active virtual environment:
 
```shell script
pip install ae-enaml-app
```

if you want to contribute to this portion then first fork
[the ae_enaml_app repository at GitLab](
https://gitlab.com/ae-group/ae_enaml_app "ae.enaml_app code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_enaml_app):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_enaml_app/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.enaml_app.html
"ae_enaml_app documentation").
